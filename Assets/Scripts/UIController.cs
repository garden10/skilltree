using System;
using Model;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using View;

public class UIController : MonoBehaviour
{
    [SerializeField] private LineBuilder _lineBuilder;
    [SerializeField] private Button _getMoneyButton;
    [SerializeField] private Button _buyButton;
    [SerializeField] private Button _sellButton;
    [SerializeField] private Button _sellAllButton;
    [SerializeField] private TextMeshProUGUI _pointsText;
    [SerializeField] private TextMeshProUGUI _cellPriceText;

    private void Awake()
    {
        _getMoneyButton.onClick.AddListener(GetMoney);
        _buyButton.onClick.AddListener(BuyCell);
        _sellButton.onClick.AddListener(SellCell);
        _sellAllButton.onClick.AddListener(SellAllCells);
    }

    private void Start()
    {
        SkillTree.Instance.PointsChanged += PointsChanged;
        PointsChanged(SkillTree.Instance.Points);
    }

    public void CellSelect(Cell cell)
    {
        _buyButton.interactable = cell.ReadyForBuy;
        _sellButton.interactable = cell.ReadyForSell;
        _cellPriceText.text = cell.Price.ToString();

    }

    private void PointsChanged(int points) =>
        _pointsText.text = points.ToString();

    private void GetMoney()
    {
        SkillTree.Instance.GetMoney();
    }
    private void BuyCell()
    {
        SkillTree.Instance.BuyCell();
        _lineBuilder.CheckLines();
    }

    private void SellCell()
    {
        SkillTree.Instance.SellCell();
        _lineBuilder.CheckLines();
    }

    private void SellAllCells()
    {
        SkillTree.Instance.SellAllSkills();
        _lineBuilder.CheckLines();
    }

    private void OnDisable()
    {
        _getMoneyButton.onClick.RemoveListener(GetMoney);
        _buyButton.onClick.RemoveListener(BuyCell);
        _sellButton.onClick.RemoveListener(SellCell);
        _sellAllButton.onClick.RemoveListener(SellAllCells);
        SkillTree.Instance.PointsChanged -= PointsChanged;
    }
}