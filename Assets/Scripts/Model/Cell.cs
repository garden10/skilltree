using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace Model
{
    [Serializable]
    public class Cell
    {
        [SerializeField] private bool _isBase;
        [SerializeField] private string _name;
        [SerializeField] private int _price;
        [NonSerialized] private AvailableStatus _availableStatus;

        [NonSerialized] public List<Cell> LinkedCells = new List<Cell>();

        private bool _readyForBuy;
        private bool _readyForSell;

        public int Price
        {
            get => _price;
            set => _price = value;
        }

        public string Name
        {
            get => _name;
            set => _name = value;
        }

        public bool IsBase
        {
            get => _isBase;
            set => _isBase = value;
        }

        public AvailableStatus Status
        {
            get => _availableStatus;
            set => _availableStatus = value;
        }

        public bool ReadyForBuy
        {
            get => _readyForBuy;
            set => _readyForBuy = value;
        }

        public bool ReadyForSell => _readyForSell;

        public Action Deselect;
        public Action NotEnoughPoints;
        public Action<AvailableStatus> StatusChanged;

        public Cell()
        {
            _availableStatus = AvailableStatus.Unavailable;
        }

        public void Init()
        {
            if (_isBase)
            {
                _availableStatus = AvailableStatus.Base;
                foreach (var cell in LinkedCells)
                {
                    cell.SetAvailableStatus();
                }
            }
        }

        public void Reset()
        {
            if (!_isBase)
            {
                _availableStatus = AvailableStatus.Unavailable;
                StatusChanged?.Invoke(_availableStatus);
            }
        }

        public void SetAvailableStatus()
        {
            _availableStatus = AvailableStatus.Available;
        }

        public void Select()
        {
            _readyForBuy = false;
            _readyForSell = false;

            if (_availableStatus == AvailableStatus.Available)
                _readyForBuy = true;

            else if (_availableStatus == AvailableStatus.Opened)
                _readyForSell = MajorRequest();

            SkillTree.Instance.SelectCell(this);
        }

        public void DeSelect() =>
            Deselect?.Invoke();

        public void Buy()
        {
            _availableStatus = AvailableStatus.Opened;

            foreach (var cell in LinkedCells)
                if (cell.Status == AvailableStatus.Unavailable)
                    cell.SetAvailableStatus();
            StatusChanged?.Invoke(_availableStatus);
            Select();
        }

        public void Sell()
        {
            _availableStatus = AvailableStatus.Available;

            foreach (var cell in LinkedCells)
                if (cell.Status == AvailableStatus.Available)
                    cell.SetUnavailableStatus();

            StatusChanged?.Invoke(_availableStatus);
            Select();
        }

        public void SetUnavailableStatus()
        {
            foreach (var cell in LinkedCells)
                if (cell.Status is AvailableStatus.Opened or AvailableStatus.Base)
                    return;
            _availableStatus = AvailableStatus.Unavailable;
        }

        private bool MajorRequest()
        {
            bool request = true;
            foreach (var cell in LinkedCells)
            {
                if (cell.Status is AvailableStatus.Unavailable or AvailableStatus.Available)
                    continue;
                request = request && cell.MinorRequest(this);
            }

            return request;
        }

        private bool MinorRequest(Cell firstCell)
        {
            var visited = new List<Cell>();
            Stack<Cell> stack = new Stack<Cell>();
            stack.Push(this);
            visited.Add(firstCell);
            visited.Add(this);

            while (stack.Count > 0)
            {
                var peekCell = stack.Peek();
                stack.Pop();

                if (peekCell.Status == AvailableStatus.Base)
                    return true;

                foreach (var linkedCell in peekCell.LinkedCells)
                {
                    if (!visited.Contains(linkedCell))
                    {
                        if (linkedCell.Status is AvailableStatus.Opened or AvailableStatus.Base)
                        {
                            stack.Push(linkedCell);
                            visited.Add(linkedCell);
                        }
                    }
                }
            }
            return false;
        }
    }
}