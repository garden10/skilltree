namespace Model
{
    public enum AvailableStatus
    {
        Unavailable,
        Available,
        Opened,
        Base
    }
}