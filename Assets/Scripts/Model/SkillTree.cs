using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

namespace Model
{
    [Serializable]
    public class SkillTree
    {
        [SerializeField] private int _points;

        [NonSerialized] private Cell _selectedCell;
        [NonSerialized] private Cell _baseCell;
        private List<Cell> _allCellsList = new List<Cell>();

        private static SkillTree _instance;
        public static SkillTree Instance => _instance;

        public List<Cell> AllCellsList
        {
            get => _allCellsList;
            set => _allCellsList = value;
        }

        public int Points
        {
            get => _points;
            set => _points = value;
        }

        public Cell BaseCell
        {
            get => _baseCell;
            set => _baseCell = value;
        }

        public Action<Cell> CellSelected;
        public Action<int> PointsChanged;

        public void Init()
        {
            _instance = this;
            PointsChanged?.Invoke(_points);
        }

        public void SelectCell(Cell cell)
        {
            _selectedCell = cell;
            
            _selectedCell.ReadyForBuy = _selectedCell.ReadyForBuy && _selectedCell.Price <= _points;
            
            CellSelected?.Invoke(_selectedCell);
            
            foreach (var cellInList in _allCellsList)
                if (cellInList != cell)
                    cellInList.DeSelect();
        }

        public void GetMoney()
        {
            _points += 1;
            if (_selectedCell != null)
            {
                var readyForBuy = _selectedCell.ReadyForBuy && _selectedCell.Price <= _points;
                CellSelected?.Invoke(_selectedCell);
            }
            PointsChanged?.Invoke(_points);
        }

        public void BuyCell()
        {
            _selectedCell.Buy();
            _points -= _selectedCell.Price;
            PointsChanged?.Invoke(_points);
        }

        public void SellCell()
        {
            _selectedCell.Sell();
            _points += _selectedCell.Price;
            PointsChanged?.Invoke(_points);
        }

        public void SellAllSkills()
        {
            foreach (var cell in _allCellsList)
            {
                if (cell.Status == AvailableStatus.Opened)
                    _points += cell.Price;
                cell.Reset();
            }
            PointsChanged?.Invoke(_points);
            _baseCell.Init();
            
            if (_selectedCell != null)
                _selectedCell.Select();
        }
    }
}