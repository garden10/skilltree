using System;
using System.Collections.Generic;
using Model;
using UnityEngine;
using UnityEngine.Serialization;

namespace View
{
    public class SkillTreeView : MonoBehaviour
    {
        [SerializeField] private SkillTree _skillTree;
        
        [SerializeField] private UIController _uiController;

        [SerializeField] private List<CellView> _cellViews = new List<CellView>();
        public List<CellView> CellViews => _cellViews;
        public SkillTree SkillTree => _skillTree;
        
        private static SkillTreeView _instance;
        public static SkillTreeView Instance => _instance;

        private int _baseCellCounter;
        private void Awake()
        {
            _skillTree.Init();
            _instance = this;
            _skillTree.CellSelected += CellSelected;
            foreach (var cellView in GetComponentsInChildren<CellView>())
            {
                if(_baseCellCounter > 0 && cellView.CellModel.IsBase)
                {
                    Debug.LogError("There is more than 1 base cell!!!");
                    return;
                }
                
                _cellViews.Add(cellView);
                _skillTree.AllCellsList.Add(cellView.CellModel);

                if (cellView.CellModel.IsBase)
                {
                    _skillTree.BaseCell = cellView.CellModel;
                    _baseCellCounter++;
                }
            }

        }

        private void CellSelected(Cell cell) => _uiController.CellSelect(cell);

        private void OnDisable() => _skillTree.CellSelected -= CellSelected;
    }
}