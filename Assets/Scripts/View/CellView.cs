using System;
using System.Collections.Generic;
using Model;
using Unity.VisualScripting;
using UnityEditor.Animations;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace View
{
    public class CellView : MonoBehaviour
    {
        [SerializeField] private CellData _cellData;

        [SerializeField] private GameObject _selectedFrame;

        [SerializeField] private SpriteRenderer _image;

        [NonSerialized] private Cell _cellModel;

        private Animator _animator;
        private static readonly int IsCellEnable = Animator.StringToHash("IsCellEnable");

        public Cell CellModel => _cellData.Cell;

        private void Awake()
        {
            _cellModel = _cellData.Cell;
            _cellModel.IsBase = _cellData.Cell.IsBase;
            _cellModel.Name = _cellData.Cell.Name;
            _cellModel.Price = _cellData.Cell.Price;
            _cellModel.Deselect += Deselect;
            _cellModel.StatusChanged += CellDisplay;

            foreach (var cellData in _cellData.LinkedCells)
                _cellModel.LinkedCells.Add(cellData.Cell);
            _cellModel.Init();

            _image.sprite = _cellData.Image;
            _animator = GetComponent<Animator>();
        }

        private void CellDisplay(AvailableStatus status)
        {
            switch (status)
            {
                case AvailableStatus.Opened:
                    _animator.SetBool(IsCellEnable, true);
                    break;
                case AvailableStatus.Available or AvailableStatus.Unavailable:
                    _animator.SetBool(IsCellEnable, false);
                    break;
            }
        }

        private void OnMouseDown() => Select();

        private void Select()
        {
            _cellModel.Select();
            _selectedFrame.SetActive(true);
        }

        private void Deselect() => _selectedFrame.SetActive(false);
    }
}