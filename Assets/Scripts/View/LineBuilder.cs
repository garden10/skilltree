using System;
using System.Collections.Generic;
using Model;
using UnityEngine;

namespace View
{
    public class LineBuilder : MonoBehaviour
    {
        [SerializeField] private Transform _lineTemplate;

        private List<int> _cellViewIndexList = new List<int>();
        private void Start() => CheckLines();

        public void CheckLines()
        {
            if(transform.childCount != 0)
                for (int i = 0; i < transform.childCount; i++)
                    Destroy(transform.GetChild(i).gameObject);
            _cellViewIndexList.Clear();

            foreach (var cell in SkillTreeView.Instance.SkillTree.AllCellsList)
                BuildLines(cell);
        }

        private void BuildLines(Cell cellModel)
        {
            var cellModelIndex = SkillTreeView.Instance.SkillTree.AllCellsList.IndexOf(cellModel);
            
            _cellViewIndexList.Add(cellModelIndex);
            
            List<int> indexList = new List<int>();
            foreach (var cell in cellModel.LinkedCells)
                indexList.Add(SkillTreeView.Instance.SkillTree.AllCellsList.IndexOf(cell));

            foreach (var index in indexList)
            {
                if(_cellViewIndexList.Contains(index))
                    continue;
                
                var cellPosition = SkillTreeView.Instance.CellViews[cellModelIndex].transform.position;
                var linkedCellCoordinate = SkillTreeView.Instance.CellViews[index].GetComponent<Transform>().position;

                Vector3[] positionsArray = { cellPosition, linkedCellCoordinate };

                var line = Instantiate(_lineTemplate, transform).GetComponent<LineRenderer>();

                line.SetPositions(positionsArray);
                
                line.startColor = Color.black;
                line.endColor = Color.black;

                var linkedCell = SkillTreeView.Instance.SkillTree.AllCellsList[index];
                PaintLines(cellModel, linkedCell, line);
            }
        }

        private void PaintLines(Cell cellModel, Cell linkedCell, LineRenderer line)
        {
            if (YellowCheck(cellModel, linkedCell) || YellowCheck(linkedCell, cellModel))
            {
                line.startColor = Color.yellow;
                line.endColor = Color.yellow;
            }

            if (GreenCheck(cellModel, linkedCell) || GreenCheck(linkedCell, cellModel))
            {
                line.startColor = Color.green;
                line.endColor = Color.green;
            }
        }

        private bool YellowCheck(Cell cell1, Cell cell2)
        {
            switch (cell1.Status)
            {
                case AvailableStatus.Base or AvailableStatus.Opened when
                    cell2.Status is AvailableStatus.Available:
                case AvailableStatus.Base or AvailableStatus.Opened when
                    cell2.Status is AvailableStatus.Base or AvailableStatus.Opened:
                    return true;
                default:
                    return false;
            }
        }

        private bool GreenCheck(Cell cell1, Cell cell2)
        {
            return cell1.Status is AvailableStatus.Base or AvailableStatus.Opened &&
                   cell2.Status is AvailableStatus.Base or AvailableStatus.Opened;
        }
    }
}