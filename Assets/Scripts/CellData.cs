using System.Collections.Generic;
using Model;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/CellData", order = 1)]
public class CellData : ScriptableObject
{
    public Cell Cell;
    public List<CellData> LinkedCells;
    public Sprite Image;
}